# Gabi

## General functionality

This crate provides a mechanism to store numbers and display them as a multiple of the chosen power of 1000 bytes or 1024 bytes, e.g. in Megabytes or Mebibytes for human readability.

Example:

```rust
let mut bb = BytesConfig::default();
let b = bb.bytes(5247 as u16);
println!("{}", b);  //  Prints "5.25 KB"
```

The number is stored internally in the same type as was provided to initialize the struct: `u16` in this example.
 
The `Bytes` structs are displayed using the preferences held in the `BytesConfig` struct that created them:

```rust
bb.set_precision(1);
println!("{}", b);  //  Prints "5.3 KB"
```

See example for more details.

*Writing this simple crate was a good exercise to learn generics, trait bounds and automated tests.*

## Unit names in each system

| Decimal | Name  |
| ------- | ----- |
| 1       | Byte  |
| 1000    | Kilo  |
| 1000^2  | Mega  |
| 1000^3  | Giga  |
| 1000^4  | Tera  |
| 1000^5  | Peta  |
| 1000^6  | Exa   |
| 1000^7  | Zetta |
| 1000^8  | Yotta |


| Decimal | Name |
| ------- | ---- |
| 1       | Byte |
| 1024    | Kibi |
| 1024^2  | Mebi |
| 1024^3  | Gibi |
| 1024^4  | Tebi |
| 1024^5  | Pebi |
| 1024^6  | Exbi |
| 1024^7  | Zebi |
| 1024^8  | Yobi |

Ze* and Yo* are outside the range of numbers that can be represented by u64. The maximum value is 1.84467440737e+19 or 18.4467440737*1000^6 which corresponds to 18.45 Exabytes (and 16.00 Exbibytes).

## Aligned output

### Not aligned

By default or through:

```rust
bb.set_aligned(false);
```

Will print:

```
99 B
999.22 KB
```

```
999 B
999.22 KiB
```

### Current implementation: padded

Through:

```rust
bb.set_aligned(true);
```

Will print:

```
     99 B
 999.22 KB
```

```
     99 B
 999.22 KiB
```

### Future option: aligned units

> not implemented

```
     99  B
 999.22 KB
```

```
     99   B
 999.22 KiB
```

### Future option: aligned decimal point

> not implemented

```
 99    B
999.22 KB
```

```
 99    B
999.22 KiB
```

### Future option: both

> not implemented

```
 99     B
999.22 KB
```

```
 99      B
999.22 KiB
```
