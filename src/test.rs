#![cfg(test)]
use crate::*;
use std::fmt::Write;
use std::u64;

#[test]
fn test_display_bytes_ga() {
    let bb = BytesConfig::default();
    // low value
    let b = bb.bytes(0 as u16);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("0 B", w);

    // High value
    let b = bb.bytes(999 as u16);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("999 B", w);
}

#[test]
fn test_display_kilobytes() {
    let bb = BytesConfig::default();

    // Low value
    let b = bb.bytes(1000 as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 KB", w);

    // High value
    let b = bb.bytes((999.99_f32 * 1000.00_f32) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("999.99 KB", w);

    // Value that should round up to next level
    let b = bb.bytes((1000.999_f32 * 1000.00_f32) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1000.00 KB", w);
}

#[test]
fn test_display_megabytes() {
    let bb = BytesConfig::default();

    // Low value
    let b = bb.bytes(1000_u32.pow(2) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 MB", w);

    // High value
    let b = bb.bytes((999.99_f32 * 1000.00_f32.powi(2)) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("999.99 MB", w);

    // Value that should round up to next level
    let b = bb.bytes((999.999_f32 * 1000.00_f32.powi(2)) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1000.00 MB", w);
}

#[test]
fn test_display_gigabytes() {
    let bb = BytesConfig::default();

    // Low value
    let b = bb.bytes(1000_u32.pow(3) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 GB", w);

    // High value
    let b = bb.bytes((999.99_f32 * 1000.00_f32.powi(3)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("999.99 GB", w);

    // Value that should round up to next level
    let b = bb.bytes((999.999_f32 * 1000.00_f32.powi(3)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1000.00 GB", w);
}

#[test]
fn test_display_terabytes() {
    let bb = BytesConfig::default();

    // Low value
    let b = bb.bytes(1000_u64.pow(4) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 TB", w);

    // High value
    let b = bb.bytes((999.99_f64 * 1000.00_f64.powi(4)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("999.99 TB", w);

    // Value that should round up to next level
    let b = bb.bytes((999.999_f64 * 1000.00_f64.powi(4)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1000.00 TB", w);
}

#[test]
fn test_display_petabytes() {
    let bb = BytesConfig::default();

    // Low value
    let b = bb.bytes(1000_u64.pow(5) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 PB", w);

    // High value
    let b = bb.bytes((999.99_f64 * 1000.00_f64.powi(5)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("999.99 PB", w);

    // Value that should round up to next level
    let b = bb.bytes((999.999_f64 * 1000.00_f64.powi(5)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1000.00 PB", w);
}

#[test]
fn test_display_exabytes() {
    let bb = BytesConfig::default();

    // Low value
    let b = bb.bytes(1000_u64.pow(6) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 EB", w);

    // Max
    let b = bb.bytes(u64::MAX);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("18.45 EB", w);
}

#[test]
fn test_display_bytes_bi() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(0 as u16);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("0 B", w);

    // High value
    let b = bb.bytes(1023 as u16);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1023 B", w);
}
#[test]
fn test_display_kibibytes() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(1024 as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 KiB", w);

    // High value
    let b = bb.bytes((1023.99_f32 * 1024.00_f32) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1023.99 KiB", w);

    // Value that should round up to next level
    let b = bb.bytes((1023.999_f32 * 1024.00_f32) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1024.00 KiB", w);
}

#[test]
fn test_display_mebibytes() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(1024_u32.pow(2) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 MiB", w);

    // High value
    let b = bb.bytes((1023.99_f32 * 1024.00_f32.powi(2)) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1023.99 MiB", w);

    // Value that should round up to next level
    let b = bb.bytes((1023.999_f32 * 1024.00_f32.powi(2)) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1024.00 MiB", w);
}

#[test]
fn test_display_gibibytes() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(1024_u32.pow(3) as u32);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 GiB", w);

    // High value
    let b = bb.bytes((1023.99_f32 * 1024.00_f32.powi(3)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1023.99 GiB", w);

    // Value that should round up to next level
    let b = bb.bytes((1023.999_f32 * 1024.00_f32.powi(3)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1024.00 GiB", w);
}

#[test]
fn test_display_tebibytes() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(1024_u64.pow(4) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 TiB", w);

    // High value
    let b = bb.bytes((1023.99_f64 * 1024.00_f64.powi(4)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1023.99 TiB", w);

    // Value that should round up to next level
    let b = bb.bytes((1023.999_f64 * 1024.00_f64.powi(4)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1024.00 TiB", w);
}

#[test]
fn test_display_pebibytes() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(1024_u64.pow(5) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 PiB", w);

    // High value
    let b = bb.bytes((1023.99_f64 * 1024.00_f64.powi(5)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1023.99 PiB", w);

    // Value that should round up to next level
    let b = bb.bytes((1023.999_f64 * 1024.00_f64.powi(5)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_ne!("1024.00 PiB", w);
}

#[test]
fn test_display_exbibytes() {
    let bb = BytesConfig::default();
    bb.set_base(BytesBase::Bibyte);

    // Low value
    let b = bb.bytes(1024_u64.pow(6) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("1.00 EiB", w);

    // Max
    let b = bb.bytes(u64::MAX);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("16.00 EiB", w);
}

#[test]
fn test_config_precision() {
    let bb = BytesConfig::default();
    bb.set_precision(3);

    // Precision of new instance
    let b = bb.bytes((555.55555 * 1000_f64.powi(3)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("555.556 GB", w);

    // Modification of precision
    bb.set_precision(0);

    // Exiting instance uses modified precision
    let mut w = String::new();
    write!(&mut w, "{}", b).unwrap();
    assert_eq!("556 GB", w);

    // New instance uses modified precision
    let b2 = bb.bytes((444.44444 * 1000_f64.powi(3)) as u64);
    let mut w = String::new();
    write!(&mut w, "{}", b2).unwrap();
    assert_eq!("444 GB", w);
}

#[test]
fn test_config_alignment() {
    let bb = BytesConfig::default();
    bb.set_aligned(true);

    let b1 = bb.bytes(1_u8);
    let mut w = String::new();
    write!(&mut w, "{}", b1).unwrap();
    assert_eq!("     1 B", w);

    let b2 = bb.bytes(11111_u16);
    let mut w = String::new();
    write!(&mut w, "{}", b2).unwrap();
    assert_eq!(" 11.11 KB", w);

    let b3 = bb.bytes(111111111_u32);
    let mut w = String::new();
    write!(&mut w, "{}", b3).unwrap();
    assert_eq!("111.11 MB", w);
}
