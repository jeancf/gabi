use super::*;

/// Specify the possible choices for the base used
/// for the display of bytes, i.e.
/// **Gabyte**: Powers of 1000 bytes (Kilobyte, Megabyte, ...)
/// **Bibyte**: Powers of 1024 bytes (Kibibyte, Mebibyte, ...)
/// giving its name to the crate Ga/Bi(byte).
#[derive(Clone, Copy)]
pub enum BytesBase {
    Gabyte,
    Bibyte,
}

impl Default for BytesBase {
    fn default() -> Self {
        BytesBase::Gabyte
    }
}

/// This struct stores the preferences for the display of bytes
/// inside BytesConfig.
#[derive(Default, Clone, Copy)]
pub(crate) struct BytesParam {
    pub(crate) base: BytesBase,
    pub(crate) precision: usize,
    pub(crate) aligned: bool,
}

/// This struct captures the preferences for display of bytes and
/// creates instances of `Bytes` struct that contain a reference
/// to the preferences that they must use for display.
/// If the preferences are changed, all `Bytes` that the `BytesConfig`
/// instanciated adopt immediately the change. Config wrapped in Mutex
/// makes the implementation threadsafe.
pub struct BytesConfig {
    pub(crate) config: Arc<Mutex<RefCell<BytesParam>>>,
}

impl BytesConfig {
    /// Specify preferences for the creation of the `BytesConfig` struct.
    pub fn new(base: BytesBase, precision: usize, padded: bool) -> Self {
        Self {
            config: Arc::new(Mutex::new(RefCell::new(BytesParam {
                base,
                precision,
                aligned: padded,
            }))),
        }
    }

    /// Specify the base used for the display of bytes: Gabyte = 1000 bytes or bibyte = 1024 bytes
    pub fn set_base(&self, b: BytesBase) {
        // Acquire lock on config and modify cell content
        let config = (*self.config).lock().unwrap();
        config.borrow_mut().base = b;
    }

    /// Specify whether the display of values is aligned or not
    pub fn set_aligned(&self, pa: bool) {
        let config = (*self.config).lock().unwrap();
        config.borrow_mut().aligned = pa;
    }

    /// Specify how many decimals to display
    pub fn set_precision(&self, pr: usize) {
        let config = (*self.config).lock().unwrap();
        config.borrow_mut().precision = pr;
    }

    /// Creates an instance of `Bytes` that stores a reference to the
    /// BytesConfig instance that created it to obtain preferences from it
    pub fn bytes<U: Unsigned + Display + PartialOrd + Into<u64> + Copy>(&self, b: U) -> Bytes<U> {
        Bytes::new(self.config.clone(), b)
    }
}

impl Default for BytesConfig {
    /// By default, capacities are displayed in Bibytes (powers of 1000), precision is set to 2 decimals
    /// and the display of values is not aligned
    fn default() -> Self {
        Self {
            config: Arc::new(Mutex::new(RefCell::new(BytesParam {
                base: BytesBase::default(),
                precision: 2,
                aligned: false,
            }))),
        }
    }
}
