use gabi::*;

fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}
fn main() {
    // Create a config struct with parameters like this:
    let _bb = BytesConfig::new(BytesBase::Bibyte, 2, false);

    // Or alternatively use defaults (default values are the same as above)
    let bb = BytesConfig::default();

    // And optionally use setters to modify parameters
    bb.set_base(BytesBase::Gabyte);
    bb.set_precision(3);
    bb.set_aligned(true);

    // Then instanciate `Bytes` structs that contains values
    // that can be displayed the chosen configuration options
    let a = bb.bytes(124 as u8);
    let b = bb.bytes(5247 as u16);
    let c = bb.bytes(156765 as u32);
    let d = bb.bytes(5259731856 as u64);

    // But not:
    // let e = bb.bytes("5");
    // let f = bb.bytes(5 as i8);
    // let g = bb.bytes(5 as f64);

    // Use into_inner to get back the internal number store (consumes the Byte instance)
    print!("{} stored as ", a);
    print_type_of(&a.into_inner()); // 124 B stored as u8

    print!("{} stored as ", b);
    print_type_of(&b.into_inner()); // 5.25 KB stored as u16

    print!("{} stored as ", c);
    print_type_of(&c.into_inner()); // 156.76 KB stored as u32

    print!("{} stored as ", d);
    print_type_of(&d.into_inner()); // 5.26 GB stored as u64

    // Create new instances
    let my_byte = bb.bytes(152585 as u32);

    // Display it with the current preferences
    println!("{}", my_byte);

    // Modify preferences
    bb.set_precision(1);
    bb.set_aligned(true);

    // Create another instance
    let my_byte2 = bb.bytes(248 as u32);

    // Display instances with modified preferences
    println!("{}", my_byte);
    println!("{}", my_byte2);
}
